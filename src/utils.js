const ISSERVER = typeof window === "undefined";

export const getIconURL = (name) => `https://www.suratfarmhouse.com/wp-content/themes/suratfarmhouse/assets/modern/partials/property-ci/img/${name}.svg`;

export const manageisUserLogin = (type) => {
  let isUserLogin = false;

  if (!ISSERVER) {
    if (type === 'set') {
      localStorage.setItem('isLogin', true);
      isUserLogin = true;
    } else if (type === 'get') {
      const isLogin  = localStorage.getItem('isLogin');
      isUserLogin = isLogin == 'true' ? true : false;
    } else if (type === 'clear') {
      localStorage.setItem('isLogin', false)
      isUserLogin = false;
    }
  }

  return isUserLogin;
}

export const deepCloneData = (data) => JSON.parse(JSON.stringify(data));