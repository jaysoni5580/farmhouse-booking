import styles from 'styles/Admin.module.scss';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import ModeEditOutlinedIcon from '@mui/icons-material/ModeEditOutlined';
import { useRouter } from 'next/router';
import { BiArrowBack } from "react-icons/bi";
import Swal from 'sweetalert2'
import { GET_FARMS, REMOVE_FARM } from 'queries/farms';
import { useLazyQuery, useMutation } from '@apollo/client';
import { useEffect } from 'react';

const Admin = () => {
	const router = useRouter();
	const [getFarms, { data, loading, refetch }] = useLazyQuery(GET_FARMS);
	const [removeFarm, { data2, loading2 }] = useMutation(REMOVE_FARM);

	useEffect(() => {
		getFarms()
	}, [])

	console.log('===>', data)

	const onEdit = (farm, index) => {
		router.push({
			pathname: '/admin/edit',
			query: {
				farmData: JSON.stringify(farm)
			}
		}, '/admin/edit')
	}

	const onDelete = (farm, index) => {
		Swal.fire({
			title: 'શું તમે ખરેખર આ ફાર્મ કાઢી નાખવા માંગો છો?',
			text: "તમે આને પાછું ફેરવી શકશો નહીં!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'ડિલિટ કરો'
		}).then((result) => {
			if (result.isConfirmed) {
				removeFarm({
					variables: {
						id: farm.id
					}
				}).then(() => refetch())
				Swal.fire(
					'Deleted!',
					'તમારું ફાર્મ કાઢી નાખવામાં આવ્યું છે.',
					'success'
				)
			}
		})
	}

	return (
		<div className={styles.mainContainer}>
			<div className={styles.innerContainer}>
				<button onClick={() => router.push('/')} className={styles.backButton}>
					<BiArrowBack className={styles.backButton} />
				</button>
				<span>Admin Panel</span>
				<div className={styles.farmsSection}>
					{!loading && data?.farms?.map((farm, index) => {
						return (
							<div className={styles.farmContainer} key={index}>
								<img src={farm.farm_photo} alt="" />
								<div className={styles.details}>
									<span>ફાર્મ: {farm.farmname}, {farm.address}</span>
									<div className={styles.buttons}>
										<div className={styles.btn} onClick={() => onEdit(farm, index)}>
											<span>Edit</span>
											<ModeEditOutlinedIcon />
										</div>
										<div className={styles.btn} onClick={() => onDelete(farm, index)}>
											<span>Delete</span>
											<DeleteOutlineOutlinedIcon />
										</div>
									</div>
								</div>
							</div>
						)
					})}
					<div className={styles.addNew} onClick={() => router.push('/admin/addNew')}>
						<span>+ Add New Farm</span>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Admin;