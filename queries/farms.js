import { gql } from '@apollo/client';

// Add Farm
export const INSERT_FARM = gql`
  mutation addFarm(
    $farmname: String!
    $address: String!
    $rent: jsonb!
    $total_person_limit: Int!
    $extra_person_charge: numeric!
    $sq_ft: String!
    $pool_sq_ft: String!
    $amenities: jsonb!
    $farm_photo: String!
    $all_photos: jsonb!
    $assets: jsonb!
    $location_link: String!
  ) {
    insert_farms_one(object: {
      farmname: $farmname
      address: $address
      rent: $rent
      total_person_limit: $total_person_limit
      extra_person_charge: $extra_person_charge
      sq_ft: $sq_ft
      pool_sq_ft: $pool_sq_ft
      amenities: $amenities
      farm_photo: $farm_photo
      all_photos: $all_photos
      assets: $assets,
      location_link: $location_link
    }){
      id
    }
  }
`;

export const EDIT_FARM = gql`
  mutation updateFarm(
    $id: Int!
    $farmname: String!
    $address: String!
    $rent: jsonb!
    $total_person_limit: Int!
    $extra_person_charge: numeric!
    $sq_ft: String!
    $pool_sq_ft: String!
    $amenities: jsonb!
    $farm_photo: String!
    $all_photos: jsonb!
    $assets: jsonb!
    $location_link: String!
  ) {
    update_farms(
    where: {id: {_eq: $id}},
    _set: {
      farmname: $farmname
      address: $address
      rent: $rent
      total_person_limit: $total_person_limit
      extra_person_charge: $extra_person_charge
      sq_ft: $sq_ft
      pool_sq_ft: $pool_sq_ft
      amenities: $amenities
      farm_photo: $farm_photo
      all_photos: $all_photos
      assets: $assets
      location_link: $location_link
    }){
      affected_rows
      returning{
        id
      }
    }
  }
`;

// Get Farms
export const GET_FARMS = gql`
  query getFarms {
    farms{
      id
      farmname
      address
      rent
      total_person_limit
      extra_person_charge
      sq_ft
      pool_sq_ft
      amenities
      farm_photo
      all_photos
      assets
      location_link
    }
  }
`;

export const REMOVE_FARM = gql`
  mutation removeFarm(
    $id: Int!
  ) {
    delete_farms(where: {id: {_eq: $id}}){
        affected_rows
      }  
  }
`