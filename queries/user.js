import { gql } from '@apollo/client';


// GET Users
export const GET_USER = gql`
  query getUser(
    $email: String!
    $password: String!
  ) {
    user(where: {
        email: {_eq: $email} 
        password: {_eq: $password}
      }) 
    {
      name
      email
    }
  }
`;
