import styles from 'styles/details.module.scss';
import { BiArrowBack } from "react-icons/bi";
import { getIconURL } from 'src/utils';
import { useRouter } from 'next/router';
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import { useEffect, useState } from 'react';

const Details = () => {
	const router = useRouter()
	const [farmDetails, setFarmDetails] = useState({});
	const { farmData } = router.query;

	useEffect(() => {
		if (farmData) {
			const data = JSON.parse(farmData)
			setFarmDetails(data)
		}
	}, [farmData])

	const IconView = ({ iconImg, iconName }) => {
		return (
			<div className={styles.iconContainer}>
				<img src={iconImg} />
				<span>{iconName}</span>
			</div>
		)
	}

	const openInNewTab = (link) => {
		window.open(link, '_blank', 'noopener,noreferrer')
	}

	// const allImages = [farmDetails?.farm_photo, ...farmDetails?.all_photos]

	console.log('====>',farmDetails)

	return farmDetails && (
		<div className={styles.mainContainer}>
			<BiArrowBack className={styles.backButton} onClick={() => router.back()} />
			<Swiper
				pagination={true}
				modules={[Pagination]}
				onSwiper={(swiper) => console.log(swiper)}
			>
				{farmDetails?.all_photos?.map((image, index) => {
					return (
						<SwiperSlide key={index} >
							<img src={image} alt="logo" width="100%" className={styles.entryGet} />
						</SwiperSlide>
					)
				})}
			</Swiper>
			<div className={styles.quickview}>
				<div className={styles.farmName}>
					<h3>{farmDetails?.farmname}</h3>
				</div>
				<div className={styles.farmPricing}>
					<table>
						<tr>
							<td>Monday To Friday</td>
							<td>Saturday To Sunday</td>
						</tr>
						<tr>
							<td>Day : &#8377; {farmDetails?.rent?.mon_fri_morning}</td>
							<td>Day : &#8377; {farmDetails?.rent?.sun_sat_morning}</td>
						</tr>
						<tr>
							<td>Night : &#8377; {farmDetails?.rent?.mon_fri_night}</td>
							<td>Night : &#8377; {farmDetails?.rent?.sun_sat_night}</td>
						</tr>
					</table>
				</div>
				<div className={styles.farmNote}>
					<span>નોંધ : દરેક તહેવારના ભાવ અલગ રહેશે, કોલ કરીને ભાવ જાણી લેવો. મોબાઈલ : +91 79845 45484</span>
				</div>
			</div>
			<div className={styles.farmAbout}>
				<span>About</span>
				<p>Digas, Surat</p>
				<table>
					<tr>
						<td>Check-In Time</td>
						<td>Check-Out Time</td>
					</tr>
					<tr>
						<td>(For 24 Hours):</td>
						<td>(For 24 Hours):</td>
					</tr>
					<tr>
						<td>7:00 pm</td>
						<td>7:00 pm</td>
					</tr>
					<tr>
						<td>(For 12 Hours):</td>
						<td>(For 12 Hours):</td>
					</tr>
					<tr>
						<td>7:00 am</td>
						<td>7:00 pm</td>
					</tr>
					<tr>
						<td>Farm Size:</td>
						<td>Swimming Pool Size:</td>
					</tr>
					<tr>
						<td>{farmDetails?.sq_ft} વાર</td>
						<td>{farmDetails?.pool_sq_ft}</td>
					</tr>
					<tr>
						<td>ટોટલ કેપેસિટી:</td>
						<td>એક્સટ્રા પર વ્યક્તિ:</td>
					</tr>
					<tr>
						<td>{farmDetails?.total_person_limit} વ્યક્તિ</td>
						<td>&#8377; 100</td>
					</tr>
				</table>
				<div className={styles.farmFecility}>
					<IconView iconImg={getIconURL('ic_refrigerator')} iconName={`${farmDetails?.assets?.kitchen} કિચન`} />
					<IconView iconImg={getIconURL('ic_bathroom')} iconName={`${farmDetails?.assets?.bathroom} બાથરૂમ`} />
					<IconView iconImg={getIconURL('ic_bed')} iconName={`${farmDetails?.assets?.bed} બેડ`} />
					<IconView iconImg={getIconURL('bed')} iconName={`${farmDetails?.assets?.khatla} ખાટલા`} />
					<IconView iconImg={getIconURL('bedroom')} iconName={`${farmDetails?.assets?.gadla} ગાદલા`} />
				</div>
			</div>
			<div className={styles.suvidhao}>
				<span>સુવિધાઓ</span>
				<div className={styles.minibox}>
					{farmDetails?.amenities?.includes('સ્વિમિંગ પૂલ') && <IconView iconImg={getIconURL('ic_swimming_pool')} iconName={'સ્વિમિંગ પૂલ'} />}
					{farmDetails?.amenities?.includes('ગાદલા') && <IconView iconImg={getIconURL('bedroom')} iconName={'ગાદલા'} />}
					{farmDetails?.amenities?.includes('ફ્રીજ') && <IconView iconImg={getIconURL('ic_refrigerator')} iconName={'ફ્રીજ'} />}
					{farmDetails?.amenities?.includes('ચૂલો') && <IconView iconImg={getIconURL('ic_stove')} iconName={'ચૂલો'} />}
					{farmDetails?.amenities?.includes('ગેસ નોબાટલો') && <IconView iconImg={getIconURL('ic_gas_bottle')} iconName={'ગેસ નોબાટલો'} />}
					{farmDetails?.amenities?.includes('વાસણ') && <IconView iconImg={getIconURL('ic_vasan')} iconName={'વાસણ'} />}
					{farmDetails?.amenities?.includes('ડાઇનીંગ ટેબલ') && <IconView iconImg={getIconURL('ic_dinning_table')} iconName={'ડાઇનીંગ ટેબલ'} />}
					{farmDetails?.amenities?.includes('ખુરશી') && <IconView iconImg={getIconURL('ic_chair')} iconName={'ખુરશી'} />}
					{farmDetails?.amenities?.includes('સોફા સેટ') && <IconView iconImg={getIconURL('ic_couch')} iconName={'સોફા સેટ'} />}
					{farmDetails?.amenities?.includes('હોલ એ.સી') && <IconView iconImg={getIconURL('ic_air_conditioner')} iconName={'હોલ એ.સી.'} />}
					{farmDetails?.amenities?.includes('ટીપાઈ') && <IconView iconImg={getIconURL('ic_tipoi')} iconName={'ટીપાઈ'} />}
					{farmDetails?.amenities?.includes('મ્યુઝીક સીસ્ટમ') && <IconView iconImg={getIconURL('ic_sound_system')} iconName={'મ્યુઝીક સીસ્ટમ'} />}
					{farmDetails?.amenities?.includes('કાર પાર્કિંગ') && <IconView iconImg={getIconURL('ic_car_parking')} iconName={'કાર પાર્કિંગ'} />}
					{farmDetails?.amenities?.includes('ખાટલા') && <IconView iconImg={getIconURL('bed')} iconName={'ખાટલા'} />}
					{farmDetails?.amenities?.includes('ગાર્ડન') && <IconView iconImg={getIconURL('ic_garden')} iconName={'ગાર્ડન'} />}
					{farmDetails?.amenities?.includes('ફિલ્ટર') && <IconView iconImg={getIconURL('ic_water_filter')} iconName={'ફિલ્ટર'} />}
				</div>
			</div>
			<div className={styles.guideline}>
				<div className={styles.heading}>
					Guidelines & rules
				</div>
				<ul>
					<li>No Alcohole Party</li>
					<li>Self Cooking</li>
					<li>Staircase only</li>
					<li>No Luggage Responsibility</li>
					<li>No Smoking</li>
					<li>Self Cleaning</li>
					<li>No Pets</li>
					<li>Non-veg not allowed</li>
				</ul>
			</div>
			<div className={styles.rules}>
				<span>નિયમો:-</span>
				<ul>
					<li>કોઈપણ વસ્તુ ભાંગ - ટુટ નો ચાર્જ અલગ થી લેવામાં આવશે.</li>
					<li>લોન ઉપર ચુંલો - તાપણું કરવું નહી.</li>
					<li>કુલ - ઝાડ ને નુકસાન કરવું નહિ.</li>
					<li>જ્યાં - ત્યાં થુંકવું નહી તથા કચરો ગમે ત્યાં ફેકવો નહી.</li>
					<li>બાથ માં કચરો નાખવો નહી તથા સાબુ- શેમ્પુ થી નાહવુ નહી.</li>
					<li>ભીના કપડાં એ મકાન માં જવું નહિ.</li>
					<li>જતી વખતે કચરા નું ઝબલું ભરી બહાર સાઈડ માં મૂકી દેવું.</li>
					<li>ઇલેકટ્રિક આઈટમ બગડી ન જાય તેની કાળજી લેવી.</li>
				</ul>
			</div>
			<div className={styles.cancel}>
				<span>કેન્સલ કરવાના નિયમ</span>
				<ul>
					<li>1 દિવસ પહેલા રદ કરવામાં આવશે તો,0% રકમ પરત કરવામાં આવશે.</li>
					<li>૩ દિવસ પહેલા રદ કરવામાં આવશે તો,75% રકમ પરત કરવામાં આવશે.</li>
					<li> દિવસ પહેલા રદ કરવામાં આવશે તો,90% રકમ પરત કરવામાં આવશે.</li>
				</ul>
			</div>
			<div className={styles.callButton}>
				<img src='/icons/phone-solid.svg' />
				<span><a href="tel:+91 79845 45484">+91 79845 45484</a></span>
			</div>
			<div className={styles.locationButton}>
				<img src='/icons/location.svg' />
				<span onClick={() => openInNewTab(farmDetails?.location_link)}>GET LOCATION</span>
			</div>
		</div>
	)
}

export default Details;